import os
from pathlib import Path
import shutil

def fast_scandir(dirname):
    subfolders = [f.path for f in os.scandir(dirname) if f.is_dir() and '.git' not in f.path and '.idea' not in f.path]
    for dirname in list(subfolders):
        subfolders.extend(fast_scandir(dirname))
    return subfolders

directories = fast_scandir('.')

src_path = './LICENSE'
for directory in directories:
    shutil.copy(src_path, directory)

print('Copied')
